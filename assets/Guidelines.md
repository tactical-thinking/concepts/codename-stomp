# Asset Guidelines

## Folder structure

```txt
assets
├── dist                # (distribution) PNG files
│   ├── arena           # each arena gets its own folder
│   │   └── temple      # (animated) sprites; bg, ground, deco, ...; example arena
│   ├── heroes          # each hero gets its own folder
│   │   └── hipster     # (animated) sprites; example character
│   └── ui              # hp, controls, etc.
└── src                 # (source) Illustrator, etc. files
```

## Sizes

* For animated sprites add `_nnn` before the file extension with `nnn` counting from `000`
* The file type for all `dist` files should be `.png`

| Filename           | Description                                                                                                       | Width (Poly) [px] | Height (Poly) [px] | Width (Pixel) [px] | Height (Pixel) [px] | Important? |
|:-------------------|:------------------------------------------------------------------------------------------------------------------|------------------:|-------------------:|-------------------:|--------------------:|:----------:|
|                    | _multiple bgs for parallax???_                                                                                    |                   |                    |                    |                     |            |
| `bg_back`          | arena background back                                                                                             | 8000              | 2000               | 400                | 100                 | !          |
| `bg_between`       | arena background middle (e.g. clouds or sth)                                                                      | ≤8000             | ≤2000              | ≤400               | ≤100                |            |
| `bg_front`         | arena background front (e.g. more clouds)                                                                         | ≤8000             | ≤2000              | ≤400               | ≤100                |            |
| `fg`               | arena foreground (e.g. leaves in front of players; don't overuse!)                                                | ≤8000             | ≤2000              | ≤400               | ≤100                |            |
| `ground`           | ground tile                                                                                                       | 500               | 200                | 25                 | 10                  | !          |
| `platform_one`     | floating platforms primary                                                                                        | ~600              | ~100               | ~30                | ~5                  | !          |
| `platform_two`     | floating platforms secondary (usually larger/smaller than primary                                                 | ~800              | ~100               | ~40                | ~5                  | !          |
|                    | _maybe randomly placed across arena; can be completely different objects per arena<br>different sizes encouraged_ |                   |                    |                    |                     |            |
| `dec_one`          | trees, bushes, etc.                                                                                               | ~500              | ~500               | ~25                | ~25                 | !          |
| `dec_two`          | trees, bushes, etc.                                                                                               | ~500              | ~500               | ~25                | ~25                 |            |
| `dec_three`        | trees, bushes, etc.                                                                                               | ~500              | ~500               | ~25                | ~25                 |            |
| `dec_four`         | trees, bushes, etc.                                                                                               | ~500              | ~500               | ~25                | ~25                 |            |
| `dec_five`         | trees, bushes, etc.                                                                                               | ~500              | ~500               | ~25                | ~25                 |            |
| `dec_mov`          | birds, fireflies, etc. (moving, animation)                                                                        |                   |                    |                    |                     |            |
| `hp_bg`            | hp bar background                                                                                                 | 1000              | 100                | 50                 | 5                   | !          |
| `hp_dmg`           | hp bar last damage/hit (moves to left)                                                                            | 1000              | 100                | 50                 | 5                   | !          |
| `hp_hp`            | hp bar remaining health (moves to left)                                                                           | 1000              | 100                | 50                 | 5                   | !          |
| `joy_bg`           | joystick background                                                                                               | 500               | 500                | 25                 | 25                  | !          |
| `joy_fg`           | joystick foreground                                                                                               | 300               | 300                | 15                 | 15                  | !          |
| `swipe_track`      | in case swipe leaves track (only one spawns & maybe fades out)                                                    | 1000              | 100                | 50                 | 5                   |            |
| `swipe_particles`  | in case swipe leaves particles (multiple spawn)                                                                   | 100               | 100                | 5                  | 5                   | !          |
|                    | _each hero has its own folder, but are named the same; replace n with frame_                                      |                   |                    |                    |                     |            |
| `hero_banner`      | from top front if you're able to do that; for character selection; ppl more likely to collect/buy/whatever        | 600               | 1000               | 30                 | 50                  |            |
| `hero_default_nnn` | default posture                                                                                                   | 350               | 550                | 18                 | 28                  | !          |
| `hero_rest_nnn`    | relaxing when standing a lil longer in one place                                                                  | ~350              | ~550               | ~18                | ~28                 |            |
| `hero_run_nnn`     | running                                                                                                           | ~350              | ~550               | ~18                | ~28                 | !          |
| `hero_turn_nnn`    | slide on quick turn                                                                                               | ~350              | ~550               | ~18                | ~28                 |            |
| `hero_melee_nnn`   | melee attack                                                                                                      | ~350              | ~550               | ~18                | ~28                 | !          |
| `hero_special_nnn` | special attack                                                                                                    | ~350              | ~550               | ~18                | ~28                 |            |
| `hero_shield_nnn`  | shield                                                                                                            | ~350              | ~550               | ~18                | ~28                 | !          |